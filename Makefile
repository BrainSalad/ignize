
.PHONY: build-backend build-backend-docker build-frontend build run-backend run-frontend test-backendtest-frontend

VERSION=0.1

help:
	@echo "build-backend             builds the ignize executable"
	@echo "build-backend-docker      builds the docker image for backend and pushes it"
	@echo "run-backend-docker        runs a container with the docker backend image"
	@echo "stop-backend-docker       stops the container started with run-backend-docker"
	@echo "build-frontend            builds the frontend"
	@echo "build                     build both the backend and the frontend"
	@echo "test-backend              runs the tests for the backend"

build-backend:
	cd backend && go build -o ignize ; cd ..

run-backend:
	cd backend && go run main.go

build-backend-docker:
	cd backend && docker build . -t clepcea/ignize:${VERSION}
	docker push clepcea/ignize:${VERSION}

run-backend-docker: stop-backend-docker
	cd backend && sed -i 's/image:.*/image: clepcea\/ignize:${VERSION} /g' docker-compose.yml; cd ..
	cd backend &&  docker-compose up -d; cd ..

stop-backend-docker:
	cd backend && docker-compose down; cd ..

build-frontend:


build: build-backend build-frontend

test-backend:
	cd backend && go test -v ./...
