package structures

import (
	"strings"
)

// Storage is the interface used by Picture to locate the actual file in the storage backend.
type Storage interface {
	GetPictures(Tag) []Picture
	GetPictureBinary(Picture) ([]byte, error)
}

// Picture represents a picture file.
type Picture struct {
	Tags         []Tag   `json:"tags"`
	Hash         string  `json:"hash"`
	FileLocation string  `json:"-"`
	Storage      Storage `json:"-"`
}

//IsPicture returns true if the fileName passed to it
//is a recognized picture
func IsPicture(fileName string) bool {
	fName := strings.ToLower(fileName)

	return strings.HasSuffix(fName, ".jpg") ||
		strings.HasSuffix(fName, ".jpeg") ||
		strings.HasSuffix(fName, ".png") ||
		strings.HasSuffix(fName, ".gif")

}

// Tag is the attribute added to a picture to classify it.
// The default tag is "Untagged"
// After the initial setup, each Picture get's a tag with the name of the Directory it is in.
type Tag struct {
	Name string `json:"name"`
}

//PictureHateOAS is used to add HateOAS data to a Picture when transforming to json
//for http response
type PictureHateOAS struct {
	Picture

	Links []map[string]string `json:"links"`
}

//PictureHateOASResponse is used to construct the HateOAS response
type PictureHateOASResponse struct {
	Embedded []PictureHateOAS    `json:"embedded"`
	Links    []map[string]string `json:"links"`
	Page     map[string]int      `json:"page"`
}
