package structures

import (
	"regexp"
)

func FilterByTag(tag Tag) func(t Picture) bool {
	return func(p Picture) bool {
		for _, t := range p.Tags {
			if t.Name == tag.Name {
				return true
			}
		}
		return false
	}
}

func FilterByTagRegex(tag Tag) func(t Picture) bool {

	return func(p Picture) bool {
		for _, t := range p.Tags {
			fits, _ := regexp.MatchString(tag.Name, t.Name)
			return fits
		}
		return false
	}
}

//FilterByFunc will filter the tpictures based on a Predicate function
func FilterByFunc(slice []Picture, predicate func(t Picture) bool) []Picture {
	rez := []Picture{}
	for _, s := range slice {
		if predicate(s) {
			rez = append(rez, s)
		}
	}

	return rez
}
