package uihttp

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"gitlab.com/BrainSalad/ignize/backend/structures"

	"github.com/gorilla/mux"

	"gitlab.com/BrainSalad/ignize/backend/configuration"
)

var config *configuration.Config

// HashTags is a temporary DB to hold the list of existing tags and to which pictures do they belong
var HashTags map[string][]structures.Tag

// LoadConfiguration creates a Configuration object based on the profided configuration file
func LoadConfiguration() error {
	var err error

	if len(os.Args) > 1 {
		config, err = configuration.NewConfig(os.Args[1])
	} else {
		config, err = configuration.NewConfig("asset/run_config.json")
	}

	val, exists := os.LookupEnv("BACKEND_URL")
	if exists {
		config.BackEndAddress = val
	}

	val, exists = os.LookupEnv("WEBVIEW_URL")
	if exists {
		config.WebViewAddress = val
	}

	return err
}

// GetPictures is the http handler for getting:
// - the list of available pictures
// - Metadata of a specific picture
func GetPictures(response http.ResponseWriter, request *http.Request) {

	vars := mux.Vars(request)
	currentHash := vars["hash"]

	if origin := request.Header.Get("Origin"); origin != "" {
		if origin == config.WebViewAddress || origin == config.BackEndAddress {
			log.Printf("Setting CORS headers for origin: %s", origin)
			response.Header().Set("Access-Control-Allow-Origin", origin)
			response.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
			response.Header().Set("Access-Control-Allow-Headers",
				"Accept,Authorization,Cache-Control,Content-Type,DNT,If-Modified-Since,Keep-Alive,Origin,User-Agent,X-Requested-With")
			response.Header().Set("Content-Type", "application/json")
		}
	}

	pictures := []structures.Picture{}

	found := false
	for _, storage := range config.Storage {
		pics := getPicturesWithTags(storage.GetPictures(structures.Tag{Name: ".*"}))
		for _, p := range pics {
			pictures = append(pictures, p)
			if currentHash != "" && currentHash == p.Hash {
				pictures = []structures.Picture{p}
				found = true
				break
			}
		}
		if found {
			break
		}
	}

	hateOasResponse := structures.PictureHateOASResponse{}

	hateOasPictures := []structures.PictureHateOAS{}

	for _, p := range pictures {
		hateOasPictures = append(hateOasPictures,
			structures.PictureHateOAS{
				Picture: p,
				Links: []map[string]string{
					{"self": "/v1/api/pictures/" + p.Hash},
					{"image": "/v1/api/pictures/image/" + p.Hash},
				}})
	}

	hateOasResponse.Embedded = hateOasPictures
	hateOasResponse.Links = nil
	hateOasResponse.Page = nil

	json.NewEncoder(response).Encode(hateOasResponse)
}

// PutPictureTags is the http handler for altering the tags of a picture
func PutPictureTags(response http.ResponseWriter, request *http.Request) {
	if origin := request.Header.Get("Origin"); origin != "" {
		if origin == config.WebViewAddress || origin == config.BackEndAddress {
			log.Printf("Setting CORS headers for origin: %s", origin)
			response.Header().Set("Access-Control-Allow-Origin", origin)
			response.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
			response.Header().Set("Access-Control-Allow-Headers",
				"Accept,Authorization,Cache-Control,Content-Type,DNT,If-Modified-Since,Keep-Alive,Origin,User-Agent,X-Requested-With")
			response.Header().Set("Content-Type", "application/json")
		}
	}

	vars := mux.Vars(request)

	data, err := ioutil.ReadAll(request.Body)

	if err != nil {
		log.Printf("Error reading input data: %s\r\n", err.Error())
		response.WriteHeader(http.StatusInternalServerError)
		return
	}

	tags := []structures.Tag{}

	err = json.Unmarshal(data, &tags)

	if err != nil {
		log.Printf("Error encoding input data to json: %s\r\n", err.Error())
		response.WriteHeader(http.StatusBadRequest)
		return
	}

	HashTags[vars["hash"]] = tags

}

// DownloadImageByHash is the http handler for fetching a specific picture's file
func DownloadImageByHash(response http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	hash := vars["hash"]
	if hash == "" {
		response.WriteHeader(http.StatusBadRequest)
		return
	}

	for _, storage := range config.Storage {
		pics := getPicturesWithTags(storage.GetPictures(structures.Tag{Name: ".*"}))
		for _, p := range pics {
			if hash == p.Hash {
				file, err := storage.GetPictureBinary(p)
				if err != nil {
					response.WriteHeader(http.StatusInternalServerError)
					return
				}
				response.Header().Set("Content-type", "image/png")
				response.Write(file)
				return
			}
		}
	}
	response.WriteHeader(http.StatusNotFound)
}

// GetPicturesWithTag is the http handler for getting a list of pictures that belogn to a specific tag
func GetPicturesWithTag(response http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	filterTag := structures.Tag{Name: vars["filter"]}

	if filterTag.Name == "" {
		response.WriteHeader(http.StatusBadRequest)
		return
	}

	filteredPictures := []structures.Picture{}

	for _, storage := range config.Storage {
		allPictures := getPicturesWithTags(storage.GetPictures(structures.Tag{Name: ""}))
		for _, picture := range allPictures {
			for _, tag := range picture.Tags {
				if tag.Name == filterTag.Name {
					filteredPictures = append(filteredPictures, picture)
				}
			}
		}
	}

	hateOasResponse := structures.PictureHateOASResponse{}
	hateOasPictures := []structures.PictureHateOAS{}

	for _, p := range filteredPictures {
		hateOasPictures = append(hateOasPictures,
			structures.PictureHateOAS{
				Picture: p,
				Links: []map[string]string{
					{"self": "/v1/api/pictures/" + p.Hash},
					{"image": "/v1/api/pictures/image/" + p.Hash},
				}})
	}

	hateOasResponse.Embedded = hateOasPictures
	hateOasResponse.Links = nil
	hateOasResponse.Page = nil

	json.NewEncoder(response).Encode(hateOasResponse)
}

// GetTags is the http handler for getting:
// - the list of all tags
// - a list of filtered tags based on given filter
func GetTags(response http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	tagsFilter := vars["filter"]

	if origin := request.Header.Get("Origin"); origin != "" {
		if origin == config.WebViewAddress || origin == config.BackEndAddress {
			response.Header().Set("Access-Control-Allow-Origin", origin)
			response.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
			response.Header().Set("Access-Control-Allow-Headers",
				"Accept,Authorization,Cache-Control,Content-Type,DNT,If-Modified-Since,Keep-Alive,Origin,User-Agent,X-Requested-With")
			response.Header().Set("Content-Type", "application/json")
		}
	}

	tags := []structures.Tag{}

	for _, tagList := range HashTags {
		for _, tag := range tagList {
			filterUp := strings.ToUpper(tagsFilter)
			tagUp := strings.ToUpper(tag.Name)
			if strings.Contains(tagUp, filterUp) {
				tags = append(tags, tag)
			}
		}
	}
	json.NewEncoder(response).Encode(tags)
}

func getPicturesWithTags(pictures []structures.Picture) []structures.Picture {

	//repopulate with Tags from memory
	for i, p := range pictures {
		if HashTags[p.Hash] != nil {
			pictures[i].Tags = HashTags[p.Hash]
		} else {
			HashTags[p.Hash] = p.Tags
		}
	}

	return pictures
}
