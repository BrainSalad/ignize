package uihttp

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func setUpRouter() http.Handler {
	router := mux.NewRouter()

	router.HandleFunc("/v1/api/pictures", GetPictures).Methods("GET")
	router.HandleFunc("/v1/api/pictures/{hash}", GetPictures).Methods("GET")
	router.HandleFunc("/v1/api/pictures/{hash}", PutPictureTags).Methods("PUT")
	router.HandleFunc("/v1/api/pictures/image/{hash}", DownloadImageByHash).Methods("GET")
	router.HandleFunc("/v1/api/pictures/tag/{filter}", GetPicturesWithTag).Methods("GET")
	router.HandleFunc("/v1/api/tags", GetTags).Methods("GET")
	router.HandleFunc("/v1/api/tags/{filter}", GetTags).Methods("GET")

	return router
}

// StartHTTPServer starts the HTTP server that serves as the backend
func StartHTTPServer() {
	router := setUpRouter()
	log.Print("Starting http server")
	log.Fatal(http.ListenAndServe(":8000", router))
}
