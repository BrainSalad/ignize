package configuration

import (
	"testing"

	"gitlab.com/BrainSalad/ignize/backend/storage"
)

func TestConfig(t *testing.T) {
	configFilePath := "../asset/test_config.json"
	config, err := NewConfig(configFilePath)
	t.Run("TestLoadConfig", func(t *testing.T) {
		if err != nil {
			t.Error("Failed to load the Config")
		}
	})

	t.Run("ConfigFilePath", func(t *testing.T) {
		if config.ConfigFile != configFilePath {
			t.Errorf("Got: %s, Wanted: %s", config.ConfigFile, configFilePath)
		}
	})

	t.Run("TestStorageLen", func(t *testing.T) {
		got := len(config.Storage)
		if got != 1 {
			t.Errorf("Config has invalid number of storage items: %d", got)
		}
	})

	localStorage, ok := config.Storage[0].(*storage.LocalStorage)
	t.Run("TestIsLocalStorage", func(t *testing.T) {
		if !ok {
			t.Error("Invalid type of Storage")
		}
	})

	t.Run("TestLocalPath", func(t *testing.T) {
		wanted := "../asset"
		if localStorage.GetPath() != wanted {
			t.Errorf("Invalid Path:\nGot: %s\nWanted: %s",
				localStorage.GetPath(), wanted)
		}
	})
}
