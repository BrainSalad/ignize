package configuration

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"

	"gitlab.com/BrainSalad/ignize/backend/storage"
	"gitlab.com/BrainSalad/ignize/backend/structures"
)

// Config stores the all the necessary configuration for running the application
type Config struct {
	Storage        []structures.Storage
	ConfigFile     string
	BackEndAddress string
	WebViewAddress string
}

//NewConfig creates a new cofiguration by reading the given file
func NewConfig(filePath string) (*Config, error) {
	c := new(Config)
	c.ConfigFile = filePath
	c.Storage = []structures.Storage{}

	c.BackEndAddress = "http://localhost:8000"
	c.WebViewAddress = "http://localhost"

	storageList, err := getStorageList(filePath)
	if err != nil {
		fmt.Print(err.Error())
		return c, err
	}

	for _, storageItem := range storageList {
		switch storageItem["type"] {
		case "LocalStorage":
			ls := storage.NewLocalStorage(storageItem["path"])
			log.Printf("Found LocalStorage config for path: %s\r\n", ls.Path)
			c.Storage = append(c.Storage, ls)
			break

		case "WebDavStorage":

			wdsc := storage.WebDavStorageConfig{
				Username: storageItem["username"],
				Password: storageItem["password"],
				BasePath: storageItem["base_path"],
				Server:   storageItem["server"],
			}

			log.Printf("Found WebDavStorage config for server: %s, base_path: %s\r\n", wdsc.Server, wdsc.BasePath)

			c.Storage = append(c.Storage, storage.NewWebDavStorage(wdsc))
			break
		default:
			return nil, fmt.Errorf("No recognized config found")
		}
	}

	return c, nil
}

func getStorageList(filePath string) ([]map[string]string, error) {

	fullConfigJSON := map[string][]map[string]string{}
	storageList := []map[string]string{}

	rawJSON, err := ioutil.ReadFile(filePath)
	if err != nil {
		fmt.Printf(err.Error())
		return storageList, err
	}
	err = json.Unmarshal(rawJSON, &fullConfigJSON)
	if err != nil {
		fmt.Print(err.Error())
		return storageList, nil
	}

	storageList = fullConfigJSON["Storage"]

	return storageList, nil
}
