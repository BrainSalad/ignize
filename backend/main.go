package main

import (
	"log"
	"os"

	"gitlab.com/BrainSalad/ignize/backend/structures"
	"gitlab.com/BrainSalad/ignize/backend/ui/http"
)

func main() {

	uihttp.HashTags = map[string][]structures.Tag{}

	err := uihttp.LoadConfiguration()

	if err != nil {
		log.Printf("Error finding config: %s\r\n", err.Error())
		os.Exit(1)
	}

	uihttp.StartHTTPServer()
}
