package storage

import (
	"path"
	"testing"

	"gitlab.com/BrainSalad/ignize/backend/structures"
)

func TestGetPictures(t *testing.T) {

	testAssetPath := "../asset"

	localStorage := NewLocalStorage(testAssetPath)

	t.Run("Check Pictures path", func(t *testing.T) {
		got := localStorage.GetPath()
		want := testAssetPath

		if got != want {
			t.Errorf("got %s, want %s", got, want)
		}
	})

	t.Run("Get pictures - empty tag", func(t *testing.T) {

		pictures := localStorage.GetPictures(structures.Tag{Name: ""})

		wantFileNames := []string{"cat1.jpg", "cat2.jpg", "dog1.jpg", "dog2.jpg"}
		for i, f := range wantFileNames {
			wantFileNames[i] = testAssetPath + "/" + f
		}

		gotFileNames := []string{}
		for _, picture := range pictures {
			gotFileNames = append(gotFileNames, picture.FileLocation)

			if !func() bool {
				for _, fileName := range wantFileNames {
					if picture.FileLocation == fileName {
						return true
					}
				}
				return false
			}() {
				t.Errorf("FileNames don't match.\nGot: %s\nWant: %s", gotFileNames, wantFileNames)
			}

			got := picture.Tags[0].Name
			want := path.Base(testAssetPath)
			if got != want {
				t.Errorf("Default tag does not match.\nGot: %s\nWant: %s", got, want)
			}
		}
	})
}

func TestGenerateHash(t *testing.T) {
	localStorage := NewLocalStorage("../asset")

	picture := structures.Picture{
		FileLocation: "../asset/cat1.jpg",
	}

	got := localStorage.generateHash(picture)
	want := "9cd7753ba371d4e47eafc98e1faf762d"

	if got != want {
		t.Errorf("Got: %s, Want: %s", got, want)
	}
}
