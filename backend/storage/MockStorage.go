package storage

import (
	"gitlab.com/BrainSalad/ignize/backend/structures"
)

type MockStorage struct {
	structures.Storage
}

// GetPictures in MockStorage returns a mock list of pictures
func (m *MockStorage) GetPictures(tag structures.Tag) []structures.Picture {
	pictures := []structures.Picture{}

	for i := 0; i < 20; i++ {
		pic := structures.Picture{}
		pic.FileLocation = "/foo/bar" + string(i)
		pic.Hash = "q1w2e3r4t5y6u7i8o9p0" + string(i)
		tag := structures.Tag{}
		tag.Name = "tag" + string(i)
		pic.Tags = append(pic.Tags, tag)
		pic.Storage = m

		pictures = append(pictures, pic)
	}

	return pictures
}

//GetPictureBinary returns the io.Reader that will pass
//the image data to the http outstream
func (wds *MockStorage) GetPictureBinary(structures.Picture) ([]byte, error) {
	return nil, nil
}
