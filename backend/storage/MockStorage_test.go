package storage

import (
	"testing"

	"gitlab.com/BrainSalad/ignize/backend/structures"
)

func TestMockStorage(t *testing.T) {

	mockStorage := new(MockStorage)

	got := mockStorage.GetPictures(structures.Tag{Name: ""})

	t.Run("GetPicturesCount", func(t *testing.T) {
		want := 20
		if len(got) != want {
			t.Errorf("Got: %d, Want: %d", len(got), want)
		}
	})
}
