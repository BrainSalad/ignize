package storage

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"gitlab.com/BrainSalad/ignize/backend/structures"
)

type MockFileInfo struct {
	mock.Mock
	os.FileInfo
}

type MockWebDavStorage struct {
	mock.Mock
}

func (m *MockFileInfo) Name() string {
	args := m.Called()
	return args.String(0)
}

func (m *MockFileInfo) Size() int64 {
	args := m.Called()
	return int64(args.Int(0))
}

func (m *MockFileInfo) Mode() os.FileMode {
	args := m.Called()
	return args.Get(0).(os.FileMode)
}

func (m *MockFileInfo) ModTime() time.Time {
	args := m.Called()
	return args.Get(0).(time.Time)
}

func (m *MockFileInfo) IsDir() bool {
	args := m.Called()
	return args.Bool(0)
}

func (m *MockFileInfo) Sys() interface{} {
	args := m.Called()
	return args.Get(0)
}

func WebDavStorageConfigToString(config WebDavStorageConfig) string {
	return fmt.Sprintf("username: \"%s\", password: \"%s\", basePath: \"%s\", server:  \"%s\"",
		config.Username,
		config.Password,
		config.BasePath,
		config.Server,
	)
}

func TestNewWebDavStorage(t *testing.T) {
	t.Run("Test the creation of a WebDavStorage from a WebDavConfig",
		func(t *testing.T) {
			config := WebDavStorageConfig{
				Username: "test",
				Password: "testpass",
				BasePath: "a path",
				Server:   "a server",
			}

			storage := NewWebDavStorage(config)

			if !reflect.DeepEqual(storage.Config, config) {
				t.Errorf("expected: %s\r\n, got %s\r\n",
					WebDavStorageConfigToString(config),
					WebDavStorageConfigToString(storage.Config),
				)
			}
		})
}

func TestNewWebDavStorageFromReader(t *testing.T) {
	t.Run("Test the creation of a WebDavStorage from a io.Reader",
		func(t *testing.T) {
			config := WebDavStorageConfig{
				Username: "test",
				Password: "testpass",
				BasePath: "a path",
				Server:   "http://noserver",
			}

			jsonBytes, err := json.Marshal(config)
			if err != nil {
				t.Fatalf("Error marshaling config: %s\r\n", err.Error())
			}

			storage, err := NewWebDavStorageFromReader(strings.NewReader(string(jsonBytes)))
			if err != nil {
				t.Fatalf("Error creating a WebDavStorage: %s\r\n", err.Error())
			}

			if !reflect.DeepEqual(storage.Config, config) {
				t.Errorf("expected: %s\r\n, got %s\r\n",
					WebDavStorageConfigToString(config),
					WebDavStorageConfigToString(storage.Config),
				)
			}
		})

	t.Run("Test the failure to create a WebDavStorage from a io.Reader",
		func(t *testing.T) {

			_, err := NewWebDavStorageFromReader(strings.NewReader("{\"server\":false}"))
			if err == nil {
				t.Fatal("Expected error while passing invalid config string, got nil")
			}
		})
}

func TestWebDavGetPicturesFromFileInfo(t *testing.T) {
	config := WebDavStorageConfig{
		Username: "user",
		Password: "userPass",
		BasePath: "oneFolder",
		Server:   "fakeServer"}

	wds := NewWebDavStorage(config)

	fiOk := new(MockFileInfo)
	fiOk.On("Name").Return("test.jpg")
	fiOk.On("IsDir").Return(false)
	fiOk.On("ModTime").Return(time.Now())
	fiNok := new(MockFileInfo)
	fiNok.On("Name").Return("test.txt")
	fiNok.On("IsDir").Return(false)
	fiNok.On("ModTime").Return(time.Now())
	fiNokDir := new(MockFileInfo)
	fiNokDir.On("Name").Return("test.png")
	fiNokDir.On("IsDir").Return(true)
	fiNokDir.On("ModTime").Return(time.Now())

	fileInfos := []os.FileInfo{fiOk, fiNok, fiNokDir}

	pictures := wds.picturesFromWebDavFileInfos(fileInfos)

	if len(pictures) != 1 {
		t.Fatalf("Expected to get 1 picture, got %d\r\n", len(pictures))
	}

	if pictures[0].FileLocation != filepath.Join(wds.Config.Server, wds.Config.BasePath, "test.jpg") {
		t.Fatalf("Expected to get a FileLocation of %s, got %s\r\n", wds.Config.BasePath, pictures[0].FileLocation)
	}

	if pictures[0].Storage != wds {
		t.Fatal("Wrong storage set up\r\n")
	}

	if len(pictures[0].Tags) != 1 {
		t.Fatalf("Expected to get 1 tag, got %d\r\n", len(pictures[0].Tags))
	}

	if pictures[0].Tags[0].Name != wds.Config.BasePath {
		t.Fatalf("Expected to get a Tag of %s, got %s\r\n", wds.Config.BasePath, pictures[0].Tags[0].Name)
	}

}

func TestIsPicture(t *testing.T) {
	files := []string{"fileOk.jpg", "FILEOK.JPEG", "fileOk.giF", "FileOK.Png", "FileNok.txt"}

	for _, fileName := range files {
		if fileName == "FileNok.txt" {
			if structures.IsPicture(fileName) {
				t.Error("File FileNok.txt should not be recognized as a Picture")
			}
		} else if !structures.IsPicture(fileName) {
			t.Errorf("File %s should be recognized as a Picture, but is not.\r\n", fileName)
		}
	}
}
