package storage

import (
	"crypto/md5"
	"encoding/hex"
	"io/ioutil"
	"log"
	"path"

	"gitlab.com/BrainSalad/ignize/backend/structures"
)

type LocalStorage struct {
	structures.Storage

	Path string
}

func NewLocalStorage(path string) *LocalStorage {
	l := new(LocalStorage)
	l.Path = path
	return l
}

//GetPictureBinary returns the io.Reader that will pass
//the image data to the http outstream
func (l *LocalStorage) GetPictureBinary(picture structures.Picture) ([]byte, error) {
	return ioutil.ReadFile(picture.FileLocation)
}

func (l *LocalStorage) GetPath() string {
	return l.Path
}

func (l *LocalStorage) GetPictures(tag structures.Tag) []structures.Picture {
	pictures := []structures.Picture{}

	fs, err := ioutil.ReadDir(l.Path)
	if err != nil {
		log.Printf("ERROR: %s", err)

		return pictures
	}

	for _, f := range fs {

		// TODO Use filter function to filter only images
		if !f.IsDir() && structures.IsPicture(f.Name()) {
			pic := structures.Picture{}
			pic.FileLocation = l.Path + "/" + f.Name()
			pic.Hash = l.generateHash(pic)
			pic.Storage = l
			pic.Tags = append(pic.Tags, structures.Tag{Name: path.Base(l.Path)})

			pictures = append(pictures, pic)
		}
	}

	return pictures
}

func (l *LocalStorage) generateHash(picture structures.Picture) string {
	data := md5.Sum([]byte(picture.FileLocation))
	return hex.EncodeToString(data[:])
}
