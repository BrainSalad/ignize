package storage

import (
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/studio-b12/gowebdav"

	"gitlab.com/BrainSalad/ignize/backend/structures"
)

//WebDavStorage implements a client for webdav storage
type WebDavStorage struct {
	structures.Storage
	client    *gowebdav.Client
	fileInfos []os.FileInfo
	hashTags  map[string][]structures.Tag
	mutex     *sync.Mutex
	Config    WebDavStorageConfig
}

//WebDavStorageConfig represents the configuration for
//the WebDavStorage instance
type WebDavStorageConfig struct {
	Username string `json:"username"`
	Password string `json:"password"`
	BasePath string `json:"base_path"`
	Server   string `json:"server"`
}

//NewWebDavStorage creates a WebDavStorage from the provided parameters
func NewWebDavStorage(config WebDavStorageConfig) *WebDavStorage {
	webDavStorage := &WebDavStorage{
		client: gowebdav.NewClient(config.Server, config.Username, config.Password),
		Config: config,
		mutex:  &sync.Mutex{},
	}

	go func() {
		webDavStorage.populatePictures()
	}()

	return webDavStorage
}

//NewWebDavStorageFromReader creates a WebDavStorage from the provided reader
func NewWebDavStorageFromReader(jsonConfig io.Reader) (*WebDavStorage, error) {

	decoder := json.NewDecoder(jsonConfig)
	config := WebDavStorageConfig{}
	err := decoder.Decode(&config)

	if err != nil {
		return nil, err
	}

	return NewWebDavStorage(config), nil

}

//GetPictureBinary returns the io.Reader that will pass
//the image data to the http outstream
func (wds *WebDavStorage) GetPictureBinary(picture structures.Picture) ([]byte, error) {

	client := http.Client{}
	a := wds.Config.Username + ":" + wds.Config.Password
	auth := "Basic " + base64.StdEncoding.EncodeToString([]byte(a))

	req, err := http.NewRequest("GET", picture.FileLocation, nil)

	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", auth)

	resp, err := client.Do(req)

	if err != nil {
		log.Printf("Error reading file on behalf of %s : %s\r\n%s", wds.Config.Username, err.Error(), picture.FileLocation)
		return nil, err
	}

	return ioutil.ReadAll(resp.Body)
}

func (wds *WebDavStorage) populatePictures() {
	defer func() {
		go func() {
			time.AfterFunc(time.Duration(10)*time.Second, wds.populatePictures)
		}()
	}()

	if wds.client == nil {
		log.Println("WebDav client uninitialized")
		return
	}

	err := wds.client.Connect()

	if err != nil {
		log.Println(err.Error())
		return
	}

	wds.fileInfos, err = wds.client.ReadDir(wds.Config.BasePath)

	if err != nil {
		log.Println(err.Error())
		return
	}

	log.Printf("WebDav found %d files\r\n", len(wds.fileInfos))
}

//GetPictures retrieves the pictures from a webdav storage
func (wds *WebDavStorage) GetPictures(tag structures.Tag) []structures.Picture {

	wds.mutex.Lock()
	defer wds.mutex.Unlock()

	pictures := structures.FilterByFunc(wds.picturesFromWebDavFileInfos(wds.fileInfos), structures.FilterByTagRegex(tag))

	return pictures
}

func (wds *WebDavStorage) picturesFromWebDavFileInfos(fileInfos []os.FileInfo) []structures.Picture {
	pictures := []structures.Picture{}

	for _, f := range fileInfos {
		if !f.IsDir() {
			if structures.IsPicture(f.Name()) {
				pict := structures.Picture{}
				pict.Tags = []structures.Tag{}
				pict.Storage = wds
				pict.FileLocation = strings.Replace(filepath.Join(wds.Config.Server, wds.Config.BasePath, f.Name()), "http:/", "http://", 1)
				pict.FileLocation = strings.Replace(pict.FileLocation, "https:/", "https://", 1)
				md5Sum := md5.Sum([]byte(pict.FileLocation + f.ModTime().String()))
				pict.Hash = hex.EncodeToString(md5Sum[:])
				pict.Tags = append(pict.Tags, structures.Tag{Name: wds.Config.BasePath})
				pictures = append(pictures, pict)
			}
		}
	}

	return pictures
}
