import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { NavTopComponent } from './main/nav-top/nav-top.component';
import { NavLeftComponent } from './main/nav-left/nav-left.component';
import { BrowseComponent } from './main/browse/browse.component';


import { SharedModule } from './shared/shared.module';
import { PictureService } from './services/picture.service';
import { FullPhotoComponent } from './main/full-photo/full-photo.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavTopComponent,
    NavLeftComponent,
    BrowseComponent,
    FullPhotoComponent
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [PictureService],
  bootstrap: [AppComponent]
})
export class AppModule { }
