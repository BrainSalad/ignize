import { Injectable, EventEmitter } from '@angular/core';
import { Tag } from '../models/tag.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Picture } from '../models/picture.model';
import { API_URL } from '../../environments/environment';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class TagsService {

  private tags: Tag[] = [];
  private tagsSearch: Tag[] = [];
  public tagsChange = new EventEmitter<Tag[]>();
  public tagsSearchResults = new EventEmitter<Tag[]>();

  constructor(private http: HttpClient) { 
    this.requestTags();
  }

  private requestTags (filter: string = null) {

    this.http.get(API_URL + `/v1/api/tags${filter ? '/' + filter : ''}`).pipe(map((data:any) => {      
      return data.map(tag => new Tag(tag));
    })).subscribe((tags: Tag[]) => {
      
      if (filter) {
        this.tagsSearch = tags;
        this.tagsSearchResults.emit(this.getTagsSearchResults());
      } else {
        this.tags = tags;
        this.tagsChange.emit(this.getTags());
      }
    })
    
  };

  public searchTag(filter:string) {
    this.requestTags(filter);
  }

  public createTag(name:string, picture:Picture) {
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    let hash = picture.hash;
    let body = picture.tags;
    body.push({"name":name});
    this.http.put(API_URL + `/v1/api/pictures/${hash}`,JSON.stringify(body), { headers: headers } ).subscribe(res => {
      this.requestTags();
    });
  }

  public getTags(): Tag[] {
    return this.tags.slice();
  };

  public getTagsSearchResults (): Tag[] {
    return this.tagsSearch.slice();
  }
}
