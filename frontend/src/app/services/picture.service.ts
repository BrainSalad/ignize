import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Picture } from '../models/picture.model';
import { API_URL } from '../../environments/environment';

@Injectable({
  	providedIn: 'root'
})
export class PictureService {  

	public picturesChange = new EventEmitter<Picture[]>();
	private pictures: Picture[] = [];
	public openPicture = new EventEmitter<Picture>();

	constructor(private http: HttpClient) {}

	public getPictures () {
		return this.pictures.slice();
	}

	public requestPictures (filter:string = null) {
		this.http.get(API_URL + `/v1/api/pictures${filter ? '/' + filter : ''}`).subscribe((data:any) => {
			if (data && data.embedded) {
				this.pictures = data.embedded.map(picture => {
					return new Picture(picture);
				})
			}
			this.picturesChange.emit(this.getPictures());			
		});
	}
}
