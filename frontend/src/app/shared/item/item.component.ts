import { Component, OnInit, Input } from '@angular/core';
import { Picture } from '../../models/picture.model';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  @Input() picture: Picture;
  @Input() links: Array<string> = [];


  constructor() {
    
  }

  ngOnInit() {}

}
