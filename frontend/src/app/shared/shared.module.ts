import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ItemComponent } from './item/item.component';
import { TagComponent } from './tag/tag.component';
import { SearchComponent } from './search/search.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule
  ],
  exports:[ItemComponent, TagComponent, SearchComponent],
  declarations: [ItemComponent, TagComponent, SearchComponent]
})
export class SharedModule { }
