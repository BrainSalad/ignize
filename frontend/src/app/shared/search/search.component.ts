import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { Tag } from '../../models/tag.model';
import { TagsService } from '../../services/tags.service';
import { Picture } from '../../models/picture.model';
import { PictureService } from '../../services/picture.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit{

  public model: any;
  private tags: Tag[];
  @Input() picture: Picture;
  @Input() action: string = 'tag';

  constructor (private tagService: TagsService, private pictureService: PictureService) {}

  ngOnInit () {
    this.tagService.tagsChange.subscribe(tags => {
      this.tags = tags;
    });
  }

  selectedItem (e?) {
    let newTag = this.model;
    if (e) {
      newTag = e.item.name;
    }
    
    if (this.action === 'pictures') {
      this.pictureService.requestPictures(newTag);
    } else {
      this.tagService.createTag(newTag, this.picture);
    }
    
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 2 ? []
        : this.tags.filter(tag => tag.name.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    );

    formatTag (x: {name: string}) {
      return x.name;
    } 
    
}
