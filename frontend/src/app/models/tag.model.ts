export class Tag {
    id: string;
    name: string;
    active: boolean

    constructor (data) {
        Object.keys(data).map(val => {
            Object.assign(this, {[val]:data[val]});
        })
    }

    private randomString(): string {
        return '_' + Math.random().toString(36).substr(2, 9);
    }
}