import { environment } from '../../environments/environment';

export class Picture {
    hash: string;
    tags: object[] = [];
    links: any[] = [];

    constructor (data) {
        Object.keys(data).map(val => {
            Object.assign(this, {[val]:data[val]});
        })
    }

    getImageUrl () {
        if (this.links.length) {
            return environment.apiUrl + this.links[1].image;
        }
    }
}