import { Component, OnInit } from '@angular/core';
import { PictureService } from '../../services/picture.service';
import { Picture } from '../../models/picture.model';

@Component({
  selector: 'app-full-photo',
  templateUrl: './full-photo.component.html',
  styleUrls: ['./full-photo.component.scss']
})
export class FullPhotoComponent implements OnInit {

  public opend: boolean = false;
  public picture: Picture = null;

  constructor(private pictureService: PictureService) { }

  ngOnInit() {
    this.pictureService.openPicture.subscribe(picture => {
      this.picture = picture;
      this.opend = true;
    })
  }

  public close() {
    this.opend = false;
    this.picture = null;
  }

}
