import { Component, OnInit } from '@angular/core';
import { chunk } from '../../helpers/utils';
import { Picture } from '../../models/picture.model';
import { Subscription } from 'rxjs';
import { PictureService } from '../../services/picture.service';

@Component({
  selector: 'app-browse',
  templateUrl: './browse.component.html',
  styleUrls: ['./browse.component.scss']
})
export class BrowseComponent implements OnInit {

  private pictures: Picture[] = [];
  public picturesList: any[] = [];
  private subscriptions: Subscription[] = [];

  constructor(private pictureService: PictureService) {}

  ngOnInit() {
    this.pictureService.requestPictures();
    this.subscriptions.push(this.pictureService.picturesChange.subscribe(pictures => {
      this.pictures = pictures;  
      this.preparePhotosList();
    }))
  }

  private preparePhotosList () {
    this.picturesList = chunk(this.pictures, 12);
  }

  public open (picture: Picture) {
    this.pictureService.openPicture.emit(picture);
  }
  

}
