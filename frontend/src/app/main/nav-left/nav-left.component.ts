import { Component, OnInit } from '@angular/core';
import { TagsService } from '../../services/tags.service';

@Component({
  selector: 'app-nav-left',
  templateUrl: './nav-left.component.html',
  styleUrls: ['./nav-left.component.scss']
})
export class NavLeftComponent implements OnInit {

  private tags;

  constructor(private tagService: TagsService) { }

  ngOnInit() {
    this.tagService.tagsChange.subscribe(tags => {
      this.tags = tags;
    });
  }

  getRandomColor () {
    return '#'+Math.floor(Math.random()*16777215).toString(16);
  }

}
