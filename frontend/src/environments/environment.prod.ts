export const environment = {
  production: true,
  apiUrl: 'http://localhost:8000'
};

export const API_URL = environment.apiUrl;